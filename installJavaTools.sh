#!/bin/bash
sudo apt-get update

if [ $(dpkg-query -W -f='${Status}' jdk 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "install jdk"
  sudo apt-get install default-jdk # Java sdk
fi

sudo apt-get clean

echo "download android tools"
wget -q https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O tools.zip

echo "create directory for Android tools"
sudo mkdir -p /opt/android-sdk-linux

echo "unzip Android tools"
sudo unzip tools.zip -d /opt/android-sdk-linux

echo "remove zip file"
rm -f tools.zip

PATH ${PATH}:/opt/android-sdk-linux/tools:/opt/android-sdk-linux/tools/bin:/opt/android-sdk-linux/platform-tools

echo "accept licences"
yes | sdkmanager --licenses

# install version of Android that ReactNative supports
sudo sdkmanager "platforms;android-23" "build-tools;23.0.1" "add-ons;addon-google_apis-google-23"
#sudo /opt/android-sdk-linux/tools/bin/sdkmanager "platforms;android-23" "build-tools;23.0.1" "add-ons;addon-google_apis-google-23"

# install a system image
#sudo /opt/android-sdk-linux/tools/bin/sdkmanager "system-images;android-23;google_apis;x86"


