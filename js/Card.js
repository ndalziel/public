"use strict";
exports.__esModule = true;
var Card = /** @class */ (function () {
    function Card(rank, suit) {
        this.rank = rank;
        this.suit = suit;
        this.name = rank + suit;
    }
    Card.prototype.getRank = function () {
        return this.rank;
    };
    Card.prototype.getSuit = function () {
        return this.suit;
    };
    Card.prototype.display = function () {
        console.log(this.name);
    };
    return Card;
}());
exports.Card = Card;
