export class Card {
    private rank: string;
    private suit: string;
    private name: string;


    constructor(rank: string, suit: string) {
        this.rank = rank;
        this.suit = suit;
        this.name = rank + suit;
    }

    public getRank() {
        return this.rank;
    }

    public getSuit() {
        return this.suit;
    }

    public display() {
        console.log(this.name);
    }
}
