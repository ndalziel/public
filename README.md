### Generate SSH Key
ssh-keygen -C "nigeldalziel@gmail.com"
creates a key in the file .ssh/id_rsa.pub

### Install xclip
sudo apt-get install xclip

### Copy SSH key
cat ~/.ssh/id_rsa.pub | xclip -sel clip

### Add key to reposoitory
https://bitbucket.org/account/user/ndalziel/ssh-keys/

### Change origin
 git remote set-url  origin git@bitbucket.org:ndalziel/public.git
 git remote add  origin git@bitbucket.org:ndalziel/public.git
