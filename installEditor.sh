#!/bin/bash
PS3='Please enter your choice: '
options=("Atom" "Brackets" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Brackets")
            if [ $(dpkg-query -W -f='${Status}' brackets 2>/dev/null | grep -c "ok installed") -eq 1 ];
            then
                echo "Brackets is already installed"
                break
            fi
            echo "Installing Brackets"
            sudo add-apt-repository ppa:webupd8team/brackets
            sudo apt-get update
            sudo apt-get install brackets
            sudo apt-get clean
            break
            ;;
        "Atom")
            if [ $(dpkg-query -W -f='${Status}' atom 2>/dev/null | grep -c "ok installed") -eq 1 ];
            then
                echo "Atom is already installed"
                break
            fi
            echo "Installing Atom"
            wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
            sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
            sudo apt-get update
            sudo apt-get install atom
            sudo apt-get clean
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
