sudo apt-get update

# Update timezone
timedatectl set-timezone America/New_York

# Cert utils
sudo apt-get install libnss3-tools

# Sound switcher
sudo apt-add-repository ppa:yktooo/ppa
sudo apt-get update && sudo apt-get install indicator-sound-switcher

# File sizes (wajig large)
sudo apt-get install wajig

# wget
sudo apt-get install apt-transport-https

#
sudo apt-get install xclip

# Remove unnecessary packages
sudo apt-get purge gnome-user-guide

sudo apt-get clean

#aws-cli
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
rm -rf awscli-bundle.zip
rm -rf awscli-bundle

sed -i '$ a PIPENV_VENV_IN_PROJECT=true' ~/.bashrc

apt install build-essential
