#!/bin/bash
sudo apt-get update

if [ $(dpkg-query -W -f='${Status}' jdk 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  sudo apt-get install default-jdk # Java sdk
fi

sudo apt-get clean

# download android tools
wget -q https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O tools.zip

# create directory for Android tools
sudo mkdir -p /opt/android-sdk-linux

# unzip Android tools
sudo unzip tools.zip -d /opt/android-sdk-linux

#remove zip file
rm -f tools.zip

sudo mkdir -p /opt/android-sdk-linux/system-images/android-23/x86
unzip x86-23_r31.zip -d /opt/android-sdk-linux/system-images/android-23/x86

# install version of Android that ReactNative supports
sudo /opt/android-sdk-linux/tools/bin/sdkmanager "platforms;android-23" "build-tools;23.0.1" "add-ons;addon-google_apis-google-23"

systemctl mask tmp.mount

cd ~
mkdir -p tmp/PackageOperation04
cd /tmp
ln -s $HOME/tmp/PackageOperation04

mkdir /opt/android-sdk-linux/tmp
export _JAVA_OPTIONS=-Djava.io.tmpdir=/opt/android-sdk-linux/tmp

sudo /opt/android-sdk-linux/tools/bin/sdkmanager "system-images;android-23;google_apis;x86"

curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
sudo bash setup_11.x
sudo apt-get install -y nodejs
sudo apt-get install npm
sudo npm install create-react-native-app -g