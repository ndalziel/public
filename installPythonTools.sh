#!/bin/bash

if [ $(dpkg-query -W -f='${Status}' python-pip 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  sudo apt-get update
  echo "Installing Pipenv"
  sudo apt-get install python-pip
  pip install --user pipenv
  echo 'PATH="$HOME/bin:$HOME/.local/bin:$PATH"' >> ~/.bashrc
  export PIPENV_VENV_IN_PROJECT="enabled"
  source ~/.bashrc
  sudo apt-get clean
fi

# if [ $(dpkg-query -W -f='${Status}' jupyter 2>/dev/null | grep -c "ok installed") -eq 0 ];
# then
#   echo "Installing Jupyter"
#   sudo pip install jupyter
#   jupyter notebook --generate-config
# fi

# The bug is found in pip 10.0.0.
#
# In linux you need to modify file: /usr/bin/pip from:
#
# from pip import main
# if __name__ == '__main__':
#     sys.exit(main())
# to this:
#
# from pip import __main__
# if __name__ == '__main__':
#     sys.exit(__main__._main())
