# Pull base image
FROM node:8-slim

# install react-native
RUN npm install -g react-native-cli

# install watchman
# need to install git
# RUN cd /tmp && git clone https://github.com/facebook/watchman.git && cd watchman && \
#     git checkout v4.1.0 && ./autogen.sh && ./configure && make && make install && rm -rf /tmp/watchman

# Set work directory
WORKDIR /code

RUN mkdir -p /usr/share/man/man1

# install java
RUN apt-get update && \
    apt-get install -y openjdk-8-jdk wget unzip && \
    rm -rf /var/lib/apt/lists/*

# set android installation directory
ENV ANDROID_HOME /opt/android-sdk-linux

# install android
RUN mkdir -p ${ANDROID_HOME} && \
    cd ${ANDROID_HOME} && \
    wget -q https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip -O android_tools.zip && \
    unzip android_tools.zip && \
    rm android_tools.zip

# add sdk tools to the path
ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools

# accept licences
RUN yes | sdkmanager --licenses

# install the version of android that react-native supports
RUN sdkmanager "platforms;android-23"

# install a system image
RUN sdkmanager "system-images;android-23;google_apis;x86"


# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
# COPY package*.json ./

# RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
# COPY . .