#!/bin/bash
sudo apt-get update

if [ $(dpkg-query -W -f='${Status}' docker.io 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "Installing Docker"
  sudo apt-get install docker.io
  sudo usermod -aG docker $USER
  sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
  sudo systemctl enable docker
else
  echo "Docker already installed"
fi

if [ $(dpkg-query -W -f='${Status}' git 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "Installing Git"
  sudo apt-get install git
  git config --global user.email "nigeldalziel@gmail.com"
  git config --global user.name "Nigel Dalziel"
else
  echo "Git already installed"
fi

sudo apt-get clean

sudo apt update
sudo apt install snapd
sudo snap install android-studio --classic
sudo snap install --classic vscode

sudo apt-get install gnome-search-tool